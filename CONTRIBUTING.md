Projektzusammensetzung

Ich habe mich dazu entschieden mich an das Einzelprojekt der "Umlautenentfernung" zu wagen, 
weil ich nicht Rechtzeitig eine passende Projektgruppe finden konnte und ich mich
aus persönlichem Interesse an die Lösung des Problems aus diesem Projekt machen möchte.
Somit kann ich versuchen meine bisherigen Kenntnisse in mein eigenes Projekt
einfließen zu lassen und die Verantwortlichkeit für den Erfolg des Projektes liegt alleine in meiner Hand.