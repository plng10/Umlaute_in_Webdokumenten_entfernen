Mein Projekt nennt sich "Umlaute entfernen" 
und in diesem Projekt widme ich mich der Entfernung von Umlauten in verschiedenen Webdokumenten.
Zur Presentation meines Projektes möchte ich die Möglichkeit haben,
Webdokumente mit Umlauten wie zum Beispiel "Ä" in "AE" umwandeln zu können.

Zu Beginn möchte ich versuchen meinen Lösungsweg mit einfachen if-Schleifen
durchsetzen zu können. Jedoch halte ich mir die Möglichkeit offen
meinen Ansatz im Laufe des Projektes zu verändern und an die aufkommenden
Problemlösungen anzupassen.